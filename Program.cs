﻿// синглтон, наблюдатель, фасад

using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Reflection.PortableExecutable;
using System.Xml.Linq;


public class Program
{
    public static void Main(string[] args)
    {     
        PlaneFlight planeFlight = new PlaneFlight("AC456", new DateTime(2023, 5, 20, 18, 30, 00));
        planeFlight.Attach(new Passenger("Олег"));
        planeFlight.Attach(new Passenger("Майкл"));

        planeFlight.FlightTime = new DateTime(2023, 5, 20, 19, 30, 00);
        planeFlight.FlightTime = new DateTime(2023, 5, 21, 0, 30, 00);

        NotifierFacade notifierFacade = NotifierFacade.Instance(planeFlight, "detail");
        NotifierFacade notifierFacade2 = NotifierFacade.Instance(planeFlight, "detail");
        if (notifierFacade == notifierFacade2)
        {
            Console.WriteLine("Singleton works, both variables contain the same instance.");
        }
        else
        {
            Console.WriteLine("Singleton failed, variables contain different instances.");
        }

        notifierFacade.UpdateFlightTime(new DateTime(2023, 5, 21, 1, 40, 00));
        notifierFacade2.UpdateFlightTime(new DateTime(2023, 5, 21, 2, 35, 00));
    }
}

public class PlaneFlight
{
    private string flight;
    private DateTime flightTime;
    private List<IPassenger> passengers = new List<IPassenger>();

    public PlaneFlight(string flight, DateTime flightTime)
    {
        this.flight = flight;
        this.flightTime = flightTime;
    }

    public void Attach(IPassenger passenger)
    {
        passengers.Add(passenger);
    }

    public void Detach(IPassenger passenger)
    {
        passengers.Remove(passenger);
    }

    public void Notify(DateTime value)
    {
        foreach (IPassenger passenger in passengers)
        {
            passenger.Update(this, value);
        }

        Console.WriteLine("");
    }

    public DateTime FlightTime
    {
        get { return flightTime; }
        set
        {
            if (flightTime != value)
            {
                Notify(value);
                flightTime = value;
            }
        }
    }

    public string Flight
    {
        get { return flight; }
    }
}


public interface IPassenger
{
    void Update(PlaneFlight flight, DateTime value);
}

public class Passenger : IPassenger
{
    private string name;
    private PlaneFlight flight;

    public Passenger(string name)
    {
        this.name = name;
    }

    public void Update(PlaneFlight flight, DateTime value)
    {
        Console.WriteLine("{0} уведомлен, что рейс {1} " +
            "перенесен с {2} на {3}", name, flight.Flight, flight.FlightTime, value);
    }

    public PlaneFlight PlaneFlight

    {
        get { return flight; }
        set { flight = value; }
    }
}

public class NotifierFacade
{
    protected PlaneFlight flight;
    protected string notifyType;
    private static NotifierFacade instance;

    protected NotifierFacade(PlaneFlight flight, string notifyType)
    {
        this.flight = flight;
        this.notifyType = notifyType;
    }

    public static NotifierFacade Instance(PlaneFlight flight, string notifyType)
    {
        if (instance == null)
        {
            instance = new NotifierFacade(flight, notifyType);
            Console.WriteLine("NotifierFacade создан");
        }
        return instance;
    }

    public void UpdateFlightTime(DateTime value)
    {
        if (this.notifyType == "detail")
        {            
            Console.WriteLine("Рейс {0} " +
            "перенесен {1}", this.flight.Flight, value - this.flight.FlightTime);
            this.flight.FlightTime = value;
        }
        else
        {
            this.flight.FlightTime = value;
        }
    }
}
